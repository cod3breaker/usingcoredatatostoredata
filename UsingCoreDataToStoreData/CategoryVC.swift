//
//  CategoryVC.swift
//  UsingCoreDataToStoreData
//
//  Created by Sunimal Herath on 27/6/19.
//  Copyright © 2019 Sunimal Herath. All rights reserved.
//

import UIKit
import CoreData

class CategoryVC: UITableViewController {
    
    var categories = [Category]()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath)
        cell.textLabel?.text = categories[indexPath.row].name
        return cell
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return categories.count
    }
    
    // MARK: - Table view delegate methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // when you select a cell in the category table view, user should be taken to the ItemsVC
        performSegue(withIdentifier: "gotoItems", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! ItemVC
        
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            destinationVC.selectedCategory = categories[selectedIndexPath.row]
        }
    }
    
    // MARK: - UI Element Methods
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var catNameTextField = UITextField()
        
        let alert = UIAlertController(title: "New Category", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Add", style: .default) { (action) in
            let newCategory = Category(context: self.context)
            newCategory.name = catNameTextField.text!
            self.categories.append(newCategory)
            
            self.saveData()
        }
        
        alert.addAction(action)
        
        alert.addTextField { (alertTextField) in
            catNameTextField = alertTextField
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Database Functions
    
    func saveData() {
        
        do{
            try context.save()
        }catch {
            print("Error saving data: \(error)")
        }
        
        tableView.reloadData()
    }
    
    func loadData() {
        let request: NSFetchRequest<Category> = Category.fetchRequest()
        do{
            categories = try context.fetch(request)
        }catch{
            print("Error fetching data: \(error)")
        }
    }
}


