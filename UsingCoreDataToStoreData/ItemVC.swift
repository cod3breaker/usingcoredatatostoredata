//
//  ItemVC.swift
//  UsingCoreDataToStoreData
//
//  Created by Sunimal Herath on 27/6/19.
//  Copyright © 2019 Sunimal Herath. All rights reserved.
//

import UIKit
import CoreData

class ItemVC: UITableViewController {

    var items = [Item]()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var selectedCategory : Category? {
        didSet{
            loadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath)
        cell.textLabel?.text = items[indexPath.row].title
        return cell
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }
    
    // MARK: - UI Element Methods

    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var itemTextField = UITextField()
        let alert = UIAlertController(title: "New Item", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Add", style: .default) { (action) in
            let newItem = Item(context: self.context)
            newItem.title = itemTextField.text!
            newItem.parentCategory = self.selectedCategory!
            self.items.append(newItem)
            self.saveData()
        }
        alert.addAction(action)
        alert.addTextField { (alertTextField) in
            itemTextField = alertTextField
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Data manipulation functions
    
    func saveData(){
        do{
            try context.save()
        }catch{
            print("Error savind item: \(error)")
        }
        tableView.reloadData()
    }
    
    func loadData(with request: NSFetchRequest<Item> = Item.fetchRequest(), predicate: NSPredicate? = nil){
        let categoryPredicate = NSPredicate(format: "parentCategory.name MATCHES %@", selectedCategory!.name!)
        
        if let additionalPredicate = predicate {
            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [categoryPredicate, additionalPredicate])
        }
        else{
            request.predicate = categoryPredicate
        }
        
        do{
            items = try context.fetch(request)
        }catch{
            print("Error loading items: \(error)")
        }
        
        tableView.reloadData()
    }
}

extension ItemVC : UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let request: NSFetchRequest<Item> = Item.fetchRequest()
        let predicate = NSPredicate(format: "title CONTAINS[cd] %@ ", searchBar.text!)
        let sortDiscriptor = NSSortDescriptor(key: "title", ascending: true)
        request.sortDescriptors = [sortDiscriptor]
        
        loadData(with: request, predicate: predicate)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 { // search bar cross pressed to clear the search term
            loadData()
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }
}
